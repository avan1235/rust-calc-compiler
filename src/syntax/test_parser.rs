#[cfg(test)]
mod test_parser {
    use syntax::*;
    use syntax::grammar::*;

    #[test]
    fn parse_a_number_atom() {
        let src = "3.14";
        let should_be = Atom::Number(3.14);

        let got = grammar::parse_Atom(src).unwrap();
        assert_eq!(got, should_be);
    }

    #[test]
    fn parse_an_identifier() {
        let src = "x";
        let should_be = Atom::Ident(String::from(src));

        let got = grammar::parse_Atom(src).unwrap();
        assert_eq!(got, should_be);
    }

    #[test]
    fn parse_a_multiply() {
        let src = "a * 5";
        let should_be = BinaryOp::mult(Atom::Ident(String::from("a")).into(), Atom::Number(5.0).into());
        let should_be = Expr::from(should_be);

        let got = grammar::parse_Expr(src).unwrap();
        assert_eq!(got, should_be);
    }

    #[test]
    fn parse_a_function_call() {
        let src = "sin(90.0)";
        let should_be = FunctionCall::new("sin", vec![Expr::Atom(Atom::Number(90.0))]);

        let got = grammar::parse_FunctionCall(src).unwrap();
        assert_eq!(got, should_be);
    }

    #[test]
    fn complex_parse_tree() {
        let src = "5 + (3-2) * x - sin(90.0)";
        let should_be = BinaryOp::sub(
            BinaryOp::add(Atom::from(5).into(),
                          BinaryOp::mult(
                              BinaryOp::sub(Atom::from(3).into(), Atom::from(2).into()).into(),
                              Atom::from("x").into(),
                          ).into()).into(),
            FunctionCall::new("sin", vec![Atom::Number(90.0).into()]).into()
        );
        let should_be = Expr::from(should_be);

        let got = grammar::parse_Expr(src).unwrap();

        assert_eq!(got, should_be);
    }
}