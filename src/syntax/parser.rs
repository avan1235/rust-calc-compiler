use lalrpop_util::ParseError;
use syntax::Expr;
use syntax::grammar::{parse_Expr, parse_Atom, parse_FunctionCall, Token};

pub fn parse<'input>(input: &'input str) -> Result<Expr, ParseError<usize, Token<'input>, &'static str>> {
    parse_Expr(input)
}