use std::convert::From;

#[derive(Debug, Clone, PartialEq)]
pub enum Atom {
    Number(f64),
    Ident(String),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Op {
    Add,
    Divide,
    Multiply,
    Subtract,
}

#[derive(Debug, Clone, PartialEq)]
pub struct FunctionCall {
    pub name: String,
    pub arguments: Vec<Expr>,
}

impl FunctionCall {
    pub fn new<S: Into<String>>(name: S, arguments: Vec<Expr>) -> FunctionCall {
        FunctionCall { name: name.into(), arguments }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Expr {
    FunctionCall(FunctionCall),
    Atom(Atom),
    BinaryOp(Box<BinaryOp>),
}

impl From<BinaryOp> for Expr {
    fn from(op: BinaryOp) -> Self {
        Expr::BinaryOp(Box::new(op))
    }
}

impl From<FunctionCall> for Expr {
    fn from(f: FunctionCall) -> Self {
        Expr::FunctionCall(f)
    }
}

impl From<Atom> for Expr {
    fn from(v: Atom) -> Self {
        Expr::Atom(v)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct BinaryOp {
    pub op: Op,
    pub left: Expr,
    pub right: Expr,
}

impl BinaryOp {
    pub fn add(l: Expr, r: Expr) -> BinaryOp {
        BinaryOp { op: Op::Add, left: l, right: r }
    }
    pub fn sub(l: Expr, r: Expr) -> BinaryOp {
        BinaryOp { op: Op::Subtract, left: l, right: r }
    }
    pub fn mult(l: Expr, r: Expr) -> BinaryOp {
        BinaryOp { op: Op::Multiply, left: l, right: r }
    }
    pub fn div(l: Expr, r: Expr) -> BinaryOp {
        BinaryOp { op: Op::Divide, left: l, right: r }
    }
}

impl From<String> for Atom {
    fn from(v: String) -> Self { Self::Ident(v) }
}

impl From<&str> for Atom {
    fn from(v: &str) -> Self { Self::Ident(String::from(v)) }
}

impl From<f64> for Atom {
    fn from(v: f64) -> Self { Self::Number(v) }
}

impl From<i64> for Atom {
    fn from(v: i64) -> Self { Self::Number(v as f64) }
}
