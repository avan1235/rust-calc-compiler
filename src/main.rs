extern crate failure;
extern crate inkwell;
extern crate lalrpop_util;

mod syntax;
mod compiler;
mod test_compiler;

fn main() {
    println!("Can use compiler::execute here :)");
    let expr = "(3 + 4)   * 6 / 3";
    println!("\"{}\" executes to: {}", expr, compiler::execute(expr).map(|v| v.to_string()).unwrap());
}
