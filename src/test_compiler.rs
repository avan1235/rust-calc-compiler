#[cfg(test)]
mod test_compiler {
    use compiler::execute;

    #[test]
    fn execute_some_binary_ops() {
        let inputs = vec![
            ("1+1", 2.0),
            ("1-1", 0.0),
            ("2*4.5", 9.0),
            ("100.0/3", 100.0 / 3.0),
        ];

        for (src, should_be) in inputs {
            let got = execute(src).unwrap();
            assert_eq!(got, should_be);
        }
    }

    #[test]
    fn execute_a_more_complex_statement() {
        let src = "5 * (100 + 3) / 9 - 2.5";
        let should_be = 5.0 * (100.0 + 3.0) / 9.0 - 2.5;

        let got = execute(src).unwrap();
        assert_eq!(got, should_be);
    }
}