use inkwell::builder::Builder;
use inkwell::context::Context;
use inkwell::module::Module;
use inkwell::OptimizationLevel;
use inkwell::targets::{InitializationConfig, Target};
use inkwell::types::FloatType;
use inkwell::values::{FloatValue, FunctionValue};

use syntax::{Atom, BinaryOp, Expr, FunctionCall, Op};
use syntax::parser::parse;

type CalcMain = unsafe extern "C" fn() -> f64;

pub fn execute(src: &str) -> Result<f64, String> {
    Target::initialize_native(&InitializationConfig::default())?;

    let ast = parse(src).map_err(|e| e.to_string())?;
    let ctx = Context::create();
    let compiler = Compiler::new(&ctx);
    let module = compiler.compile(&ast);
    let ee = module.create_jit_execution_engine(OptimizationLevel::None).map_err(|e| e.to_string())?;

    unsafe {
        let calc_main = ee.get_function::<CalcMain>("calc_main").map_err(|e| e.to_string())?;
        Ok(calc_main.call())
    }
}

struct Compiler<'ctx> {
    ctx: &'ctx Context,
    builder: Builder<'ctx>,
    double: FloatType<'ctx>,
}

impl<'ctx> Compiler<'ctx> {
    fn new(ctx: &'ctx Context) -> Compiler<'ctx> {
        let double = ctx.f64_type();
        let builder = ctx.create_builder();

        Compiler { ctx, builder, double }
    }

    fn compile(&self, ast: &Expr) -> Module {
        const CALC_ENTRYPOINT: &'static str = "calc_main";
        let mut module = self.ctx.create_module("calc");
        self.compile_function(&mut module, CALC_ENTRYPOINT, ast);
        module
    }

    fn compile_function(&self, module: &mut Module<'ctx>, name: &str, body: &Expr) -> FunctionValue {
        let sig = self.double.fn_type(&[], false);
        let func = module.add_function(name, sig, None);

        let entry = self.ctx.append_basic_block(func, "entry");
        self.builder.position_at_end(entry);

        let ret = self.compile_expr(body);

        self.builder.build_return(Some(&ret));

        func
    }

    fn compile_expr(&self, expr: &Expr) -> FloatValue {
        match expr {
            Expr::Atom(ref atom) => self.compile_atom(atom),
            Expr::BinaryOp(ref op) => self.compile_binary_op(op),
            Expr::FunctionCall(ref call) => self.compile_function_call(call),
        }
    }
    fn compile_atom(&self, atom: &Atom) -> FloatValue {
        match *atom {
            Atom::Number(n) => self.double.const_float(n),
            _ => unimplemented!(),
        }
    }
    fn compile_binary_op(&self, op: &BinaryOp) -> FloatValue {
        let left = self.compile_expr(&op.left);
        let right = self.compile_expr(&op.right);
        match op.op {
            Op::Add => self.builder.build_float_add(left, right, "add"),
            Op::Subtract => self.builder.build_float_sub(left, right, "sub,"),
            Op::Multiply => self.builder.build_float_mul(left, right, "mul"),
            Op::Divide => self.builder.build_float_div(left, right, "div"),
        }
    }
    fn compile_function_call(&self, call: &FunctionCall) -> FloatValue {
        unimplemented!()
    }
}