extern crate lalrpop;

fn main() {
    const GRAMMAR_SOURCE: &'static str = "src/syntax/grammar.lalrpop";
    lalrpop::Configuration::new()
        .set_out_dir(".")
        .process_file(GRAMMAR_SOURCE)
        .unwrap();
    println!("cargo:rerun-if-changed={}", GRAMMAR_SOURCE);
}
